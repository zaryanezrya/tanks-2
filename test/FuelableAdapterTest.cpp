#include <gtest/gtest.h>

#include <iostream>

#include "UObject.h"
#include "FuelableAdapter.h"

TEST(FuelableAdapterTestSuite, getPropertyNoErrorTestCase) 
{
    UObject uo;

    int fuel_level = 5;
    uo.setProperty("fuel_level", fuel_level);

    FuelableAdapter<int> uo_a(uo);

    ASSERT_EQ(uo_a.get_fuel_level(), fuel_level);
}

TEST(FuelableAdapterTestSuite, getPropertyErrorTestCase) 
{
    UObject uo;

    FuelableAdapter<int> uo_a(uo);

    ASSERT_THROW(uo_a.get_fuel_level(), std::bad_any_cast);
}
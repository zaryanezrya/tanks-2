#pragma once

#include "base/IUObject.h"
#include "base/ICommand.h"

#include "FuelableAdapter.h"

template <typename T>
class BurnFuelCommand : public ICommand {
 private:
  IUObject &m_tank;
  T m_to_burn;

 public:
  BurnFuelCommand(IUObject &tank, T to_burn) : m_tank(tank), m_to_burn(to_burn){};

  void execute() {
    FuelableAdapter<T> fuelable(m_tank);
    T current_level = fuelable.get_fuel_level();
    T new_level = current_level - m_to_burn;
    if (new_level < 0) {
      throw CommandException();
    }
    fuelable.set_fuel_level(new_level);
  }
};
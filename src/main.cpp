#include <iostream>
#include <queue>
#include <any>

#include "my.h"
#include "UObject.h"
#include "BurnFuelCommand.h"

int main() {
  // Tank t;
  // t.set_fuel_level(25);
  // std::cout << t.get_fuel_level() << std::endl;

  // BurnFuelCommand bf_command(t, 10);
  // CheckFuelCommand cf_command(t, 10);

  // CheckPandRunC cabf_command(&cf_command, &bf_command);

  // cabf_command.execute();
  // cabf_command.execute();
  // cabf_command.execute();
  // std::cout << t.get_fuel_level() << std::endl;

  // t.set_fuel_level(100);
  // std::cout << t.get_fuel_level() << std::endl;

  // BurnFuelCommand bf_command(t, 10);
  // //bf_command.execute(t, 10);
  // //bf_command.execute();
  // //std::cout << t.get_fuel_level() << std::endl;

  // FillFuelCommand ff_command(t, 200);
  // // ff_command.execute(t, 200);
  // //ff_command.execute();
  // //std::cout << t.get_fuel_level() << std::endl;

  // std::queue<ICommand*> q;
  // q.push(&bf_command);
  // q.push(&bf_command);
  // q.push(&bf_command);
  // q.push(&bf_command);
  // q.push(&bf_command);
  // q.push(&bf_command);
  // q.push(&bf_command);
  // q.push(&bf_command);
  // //q.push(&bf_command);
  // //q.push(&bf_command);
  // //q.push(&bf_command);
  // q.push(&ff_command);

  // MacroCommand mc(q);

  // mc.execute();

  // std::cout << t.get_fuel_level() << std::endl;

  UObject uo;

  uo.setProperty("fuel_level", 5);

  FuelableAdapter<int> uo_a(uo);
  std::cout << uo_a.get_fuel_level() << std::endl;

  BurnFuelCommand<int> bf_command(uo, 10);
  bf_command.execute();

  std::cout << uo_a.get_fuel_level() << std::endl;

  std::cout << "wroom wroom" << std::endl;
  return 0;
}

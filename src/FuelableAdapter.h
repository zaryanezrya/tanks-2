#pragma once

#include "base/IUObject.h"
#include "game/IFuelable.h"


template <typename T>
class FuelableAdapter: public IFuelable<T> {
private:
  IUObject& m_obj;

public:
  FuelableAdapter(IUObject& obj): m_obj(obj) {};

  void set_fuel_level(T new_level) {
    m_obj.setProperty("fuel_level", new_level);
  }

  T get_fuel_level() {
    return std::any_cast<T>(m_obj.getProperty("fuel_level"));
  }

  ~FuelableAdapter() = default;
};
#include "UObject.h"

std::any UObject::getProperty(const std::string& key){
    return m_store[key];
}

void UObject::setProperty(const std::string& key, const std::any& value) {
    m_store[key] = value;
}
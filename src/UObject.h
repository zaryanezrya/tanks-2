#pragma once

#include <map>

#include "base/IUObject.h"


class UObject: public IUObject {
public:
  std::any getProperty(const std::string& key);
  void setProperty(const std::string& key, const std::any& value);
  UObject() = default;

private:
  std::map<std::string, std::any> m_store;
};

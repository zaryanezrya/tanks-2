#pragma once

#include <queue>
#include <string>
#include <any>
#include <map>

#include "base/ICommand.h"
#include "base/IUObject.h"
#include "FuelableAdapter.h"


// class FillFuelCommand : public ICommand {
//  private:
//   Tank &m_tank;
//   int m_to_fill;

//  public:
//   FillFuelCommand(Tank &tank, int to_fill) : m_tank(tank), m_to_fill(to_fill) {}

//   void execute() {
//     int current_level = m_tank.get_fuel_level();
//     int new_level = current_level + m_to_fill;
//     m_tank.set_fuel_level(new_level);
//   }
// };

// class MacroCommand : public ICommand {
//  private:
//   std::queue<ICommand *> &m_q;

//  public:
//   MacroCommand(std::queue<ICommand *> &q) : m_q(q) {}
//   void execute() {
//     while (m_q.size()) {
//       auto cmd = m_q.front();
//       cmd->execute();
//       m_q.pop();
//     }
//   }
// };

// class CheckPandRunC: public ICommand {
//   private:
//     ICommand *m_p;
//     ICommand *m_c;
//   public:
//   CheckPandRunC(ICommand* p, ICommand* c): m_p(p), m_c(c) {}

//   void execute() {
//     try {
//       m_p->execute();
//     } 
//     catch (CommandException) {
//       std::cout << "An Exception" << std::endl;
//       return;
//     }
//     m_c->execute();
//   }
// };

// class CheckFuelCommand: public ICommand {
//   private:
//     Tank &m_tank;
//     int m_to_burn;
//   public:

//   CheckFuelCommand(Tank &tank, int to_burn): m_tank(tank), m_to_burn(to_burn) {}

//   void execute() {
//     int cl = m_tank.get_fuel_level();
//     if (cl - m_to_burn < 0) {
//       throw CommandException();
//     }
//   }
// };

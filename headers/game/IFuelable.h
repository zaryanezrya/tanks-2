#pragma once

template <typename T>
class IFuelable {
public:
  virtual void set_fuel_level(T new_level) = 0;
  virtual T get_fuel_level() = 0;
  virtual ~IFuelable() = default;
};
